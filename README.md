<table style="width:100%">
    <tr>
        <td align="center" valign="middle">
        <h1>Dinamičko upravljanje protokom podataka kroz streaming servis</h1>
        <h2>Bachelor rad</h2>
        <h2>Nenad Mišić ( SW-31/2016 )</h2>
        Repozitorijum sa implementacijom rešenja dinamičkog upravjlanja protokom podataka kroz streaming servis.
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle">
        <h2>Consumer</h2>
            <p>Implementacija klijentske aplikacije, odnosno <i>consumer</i> komponente nalazi se unutar <i>client-websocket/public</i> direktorijuma. Sva logika izvršavanja koda nalazi se u main.js skripti. </p>
        <br>
        <h2>Producer</h2>
            <p>Relevantna implementacija serverske aplikacije, odnosno <i>producer</i> komponente, nalazi se unutar <i>server-websocket-node-intensive-workers</i> direktorijuma.
            U direktorijumu se nalaze dve datoteke, <i>server.js</i> koja se izvršava unutar glavne niti i odgovorna je za pokretanje <i>worker.js</i> datoteke unutar <i>worker</i> niti. <i>server.js</i> skripta takođe pokreće <i>websocket</i> i <i>http</i> servere, osluškuje primljene zahteve i šalje klijentu odgovore. <i>worker.js</i> skripta ima zadatak da meri vreme u beskonačnoj petlji, i po zadatoj frekvenciji isporučuje glavnoj niti vrednosti koje simulira.</p>
        <br>
        <h2>Prethodne implementacije</h2>
            <p> Prethodne implementacije <i>producer</i> komponente, uključujući implementacije u programskim jezicima <i>Go</i> i <i>Python</i> nalaze se unutar direktorijuma <i>previous-versions</i>. S obzirom da su ove implementacije samo pokušaji u toku razvoja, kod nije preterano uredan i iskomentarisan.</p>
        <br>
        <h2>Heroku</h2>
            <p> Da bi se izbegle komplikacije oko pokretanja aplikacije, postoji verzija dostupna za pregled, a njoj se može pristupiti preko linka:</p>
            <a href="https://misic-nenad-bachelor.herokuapp.com/">https://misic-nenad-bachelor.herokuapp.com/</a>
            <p style="font-size: 8pt">* Napomena - U kasnijim večernjim satima se smanje performanse <i>Heroku</i> servera, pa su frekvencije slanja dosta manje od očekivanih</p>
        </td>
    </tr>
</table>




