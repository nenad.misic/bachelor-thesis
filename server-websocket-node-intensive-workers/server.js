// This file is executed by main thread (worker)
// It is used to initialize and run a server (both websocket and http)
// Also, it creates a worker that simulates the signal (and terminates it upon closing the connection)

const { Worker } = require('worker_threads');

var config = {
    allowedOrigins: ['http://localhost:1337', 'http://nenad-misic-bachelor.herokuapp.com', 'https://nenad-misic-bachelor.herokuapp.com'],
    frequency: 1000,
} 

// Initialization of ws and http server
var WebSocketServer = require('websocket').server;
var http = require('http');

// Initialization of SharedArrayBuffer for communication with worker
var buffer = new SharedArrayBuffer(4);
var ibuffer = new Int32Array(buffer);
ibuffer[0] = config.frequency;
 
// Http server setup (CORS and endpoints)
var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Request-Method', '*');
	response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	response.setHeader('Access-Control-Allow-Headers', '*');
    switch (request.url) {
        case '/frequency':
            // Return the current frequency
            response.writeHead(200, {'Content-Type': 'application/json'});
            response.end(JSON.stringify({Value: config.frequency}));
            break;
        case '/slow':
            // Endpoint for reducing the frequency by 20%
            response.writeHead(200, {'Content-Type': 'application/json'});
            old = config.frequency;
            config.frequency = Math.round(config.frequency * 0.8);
            ibuffer[0] = config.frequency;
            response.end(JSON.stringify({From: old, To:config.frequency}));
            break;
        case '/speed':
            // Endpoint for increasing the frequency by 20%
            response.writeHead(200, {'Content-Type': 'application/json'});
            old = config.frequency;
            config.frequency = Math.round(config.frequency * 1.2);
            ibuffer[0] = config.frequency;
            response.end(JSON.stringify({From: old, To:config.frequency}));
            break;
        default:
            // Unknown endpoint (shouldn't happen)
            response.writeHead(404);
            response.end();
            break;
    }
});

// Start the server and console log it
server.listen(process.env.port || 3000, function() {
    console.log((new Date()) + ' Server is listening on port ' + (process.env.port || 3000));
});
 

// Web socket server setup
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
    return config.allowedOrigins.indexOf(origin)!=-1;
}
 
// On request received by websocket server (connection open by client)
wsServer.on('request', function(request) {

    // Handle origins
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    
    // Accept the connection
    var connection = request.accept();

    // Initialize a worker to simulate the signal and pass the SharedArrayBuffer for communication
    const w = new Worker(require.resolve('./worker.js'), { workerData: {buffer}});
    
    // Upon receiving the signal from worker, send it through websocket
    w.on('message', data => connection.sendUTF(data));

    // If clients closes the connection, kill the worker process and console log about it
    connection.on('close', function(reasonCode, description) {
        w.terminate();
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
