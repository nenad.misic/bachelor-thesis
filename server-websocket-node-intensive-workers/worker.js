// This file is used as a worker process
// The worker should infinitely loop, calculate the time in every iteration, and according to given frequency send signal when needed
// The signal is a simulation of Math.sin

// As frequency is a dynamic field and could be changed during the execution of infinite loop, it's passed as a SharedArrayBuffer of length 4 (shared memory space with main worker)


const {parentPort, workerData } = require('worker_threads');
var present = require('present');

// Interpreting SharedArrayBuffer as Int32Array
var ib = new Int32Array(workerData.buffer);

// Return the current frequency as first (and only) element of array buffer
function getFrequency() { 
    return ib[0];
}


var timeConnected = present()
var lastTime = timeConnected;
while (true) {
    // For given frequency, calculate the time needed between two signals
    // Has to be inside the loop as frequency can change in runtime
    // Cannot be implemented using hooks or callbacks as worker is blocked by infinite loop and cannot receive message
    var timeBetweenTwoReq = 1000/getFrequency();
    let currentTime = present();

    // If calculated time has passed, signal should be sent to main worker
    if(currentTime >= lastTime + timeBetweenTwoReq){
        // Reset the timer
        lastTime = currentTime;
        // and send the message to main worker
        parentPort.postMessage( Math.sin((currentTime - timeConnected)/1000))
    }
}

