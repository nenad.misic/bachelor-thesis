// --------------------------------------------------------------------------------------------------------------------------------------------
// Variable initialization
// --------------------------------------------------------------------------------------------------------------------------------------------

// User variables
var monitor_hz = 144;
var stabilization_time = 4000;

// Private variables
var baseurlws = "ws://localhost:8080";
var baseurlhttp = "http://localhost:8080";
// var baseurlws = "wss://bachelor-golang.herokuapp.com";
// var baseurlhttp = "https://bachelor-golang.herokuapp.com";
var enableChart = true;
var enableRegulation = true;
var frequency = 0;
var nthRequest = 0;
var lastMessageTime = -1;
var lastSecondMessageTime = -1;
var numOfMessages = 0;
var changeTime = -1;
var lastRegulationTime = (new Date()).getTime();


// --------------------------------------------------------------------------------------------------------------------------------------------
// Helper functions
// --------------------------------------------------------------------------------------------------------------------------------------------
// Scroll logs to end

function checkNetworkLoad(current, last) {
    if (!enableRegulation) return;
    if (current - last - 1000 >= 50) {
        axios.get(`${baseurlhttp}/slow`).then(res => {
            slowDown(+res.data.From, +res.data.To);
        })
    }
}

function rememberTimeOfLastRegulation() {
    lastRegulationTime = (new Date()).getTime();
}

// Decrease the frequency
function slowDown(from, to) { 
    rememberTimeOfLastRegulation();
    // Add change to log
    addLogEntry(true, from, to)
    // Reset the counter that counts one second on server (it gets messed up on first second after change)
    resetSecondCounter();
    // As frequency is changed, update it
    updateFrequencyAndNthRequest(to);
}

// Increase the frequency
function speedUp(from, to) {
    rememberTimeOfLastRegulation();
    // Add change to log
    addLogEntry(false, from, to)
    // Reset the counter that counts one second on server (it gets messed up on first second after change)
    resetSecondCounter();
    // As frequency is changed, update it
    updateFrequencyAndNthRequest(to);
}

function updateScroll(){
    var element = document.getElementById("logs");
    element.scrollTop = element.scrollHeight;
}

// Add frequency change log entry
function addLogEntry(slowdown, from, to) {
    document.getElementById('logs').innerHTML += (`<br><div class="m-1"><span class="${slowdown?'slowdown':'speedup'}">${slowdown?'Slow down':'Speed up'}</span> Frequency changed from: ${Math.round(from)} to: ${Math.round(to)}</div>`); 
    setTimeout(()=>document.querySelector('.m-1:last-child').style.backgroundColor = "#ffffff", 1000);
    updateScroll();
}

// Reset the counter that counts time between two messages that were apart 1 second on server
function resetSecondCounter() {
    lastSecondMessageTime = -1;
    document.getElementById('last-second').innerHTML = '? ms';
}

// Update frequency to given value and calculate nthRequest variable to match monitor fps 
// Update input fields on front
function updateFrequencyAndNthRequest(freq) {
    frequency = Math.round(freq);
    nthRequest = Math.floor(frequency/monitor_hz);
    updateInputFields()
}

// Update input fields on front
function updateInputFields() {
    document.getElementById('frequency').value = frequency;
    document.getElementById('nthChart').value = nthRequest;
}


// Get the frequency from server and setup the socket
axios.get(`${baseurlhttp}/frequency`).then(res => {

    // --------------------------------------------------------------------------------------------------------------------------------------------
    // Variable initializations
    // --------------------------------------------------------------------------------------------------------------------------------------------
    frequency = res.data.Value;
    nthRequest = Math.floor(frequency/monitor_hz);
    var socket = new WebSocket(baseurlws);


    // Update front according to received frequency from server
    updateInputFields();

    
    // --------------------------------------------------------------------------------------------------------------------------------------------
    //Event listeners for actions on front
    // --------------------------------------------------------------------------------------------------------------------------------------------

    // nthRequest change
    document.getElementById('nthChart').addEventListener('change', (e) => {
        nthRequest = e.target.value;
    })

    // Enable chart checkbox click
    document.getElementById('enableChart').addEventListener('click', (e) => {
        enableChart = !enableChart;
    })

    // Enable regulation checkbox click
    document.getElementById('enableRegulation').addEventListener('click', (e) => {
        enableRegulation = !enableRegulation;
    })

    // Slow down frequency button click
    document.getElementById('slowFreq').addEventListener('click', (e) => {
        axios.get(`${baseurlhttp}/slow`).then(res => {
            slowDown(+res.data.From, +res.data.To);
        })
    })

    // Speed up frequency button click
    document.getElementById('speedFreq').addEventListener('click', (e) => {
        axios.get(`${baseurlhttp}/speed`).then(res => {
            speedUp(+res.data.From, +res.data.To);
        })
    })

    // Close connection button click
    document.getElementById('close-connection').addEventListener('click', () => socket.close())


    
    // --------------------------------------------------------------------------------------------------------------------------------------------
    // Chart rendering logic
    // --------------------------------------------------------------------------------------------------------------------------------------------

    // Variable initialization
    var dataArr = []

    // Dimension setup
    var margin = {top: 10, right: 30, bottom: 30, left: 60},
        width = 460 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    // Function called to render chart with given data array
    // data -> array of elements {date: int, value: float}
    function renderChart(data) {
        // Delete everything from chart
        d3.select("#chart").selectAll("*").remove()

        // Create an svg element with given dimensions and position it
        var svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                
        // Create an x axis scale
        var x = d3.scaleTime()
            .domain(d3.extent(data, function(d) { return d.date; }))
            .range([ 0, width ]);

        // Add bottom axis with given scale to chart
        svg.append("g")
            .attr("transform", "translate(0," + height/2 + ")")
            .call(d3.axisBottom(x).tickSize(0).tickFormat(""))

        // Create an y axis scale
        var y = d3.scaleLinear()
            .domain([-1, 1])
            .range([ height, 0 ]);

        // Add left axis with given scale to chart
        svg.append("g")
            .call(d3.axisLeft(y));

        // Append line to chart according to data passed to function
        svg.append("path")
            .attr('class', 'chartpath')
            .datum(data)
            .attr("fill", "none")
            .attr("stroke", "steelblue")
            .attr("stroke-width", 1.5)
            .attr("d", d3.line()
                .x(function(d) { return x(d.date) })
                .y(function(d) { return y(d.value) })
            )
    }

    // Function used to add another point to chart
    // Rerenders the whole chart
    // data -> array of elements {date: int, value: float}
    function appendPointToChart(data) {

        // Create an x axis scale
        var x = d3.scaleTime()
            .domain(d3.extent(data, function(d) { return d.date; }))
            .range([ 0, width ]);

        // Create an y axis scale
        var y = d3.scaleLinear()
            .domain([-1, 1])
            .range([ height, 0 ]);
        
        // Rerender the whole chart according to data passed to function
        d3.select('svg').select(".chartpath")
            .datum(data)
            .attr("fill", "none")
            .attr("stroke", "steelblue")
            .attr("stroke-width", 1.5)
            .attr("d", d3.line()
                .x(function(d) { return x(d.date) })
                .y(function(d) { return y(d.value) })
            )
    }


    
    // --------------------------------------------------------------------------------------------------------------------------------------------
    // Socket set-up
    // --------------------------------------------------------------------------------------------------------------------------------------------

    socket.onopen = function (event) {
        socket.send('connect');
        document.getElementById('logs').innerHTML += ("Opened"+ "<hr>"); 
        lastMessageTime = (new Date()).getTime();
        lastSecondMessageTime = (new Date()).getTime();
        changeTime = (new Date()).getTime();
    };

    socket.onmessage = (() => {
        let reqCounter = 0;
        return function (event) {
            // Increment counters of messages
            
            // reqCounter is used for counting every nth point to be added to chart
            reqCounter++;
            // numOfMessages is used for counting messages that should be 1 second appart on server
            numOfMessages++;
            let currentMessageTime = (new Date()).getTime();
            // Update time between two messages
            document.getElementById('last-message').innerHTML = `${currentMessageTime - lastMessageTime} ms`;
            lastMessageTime = currentMessageTime;

            // If one second on server has passed
            if(numOfMessages%frequency == 0) {
                if(lastSecondMessageTime != -1){
                    // Show how much time has passed on client (should be 1000ms +- 2% if network is okay)
                    document.getElementById('last-second').innerHTML = `${currentMessageTime - lastSecondMessageTime} ms`;

                    if(currentMessageTime - lastRegulationTime > stabilization_time){
                        // Check if something is wrong with network
                        checkNetworkLoad(currentMessageTime, lastSecondMessageTime);
                    }
                } 
                
                // Reset the timer
                lastSecondMessageTime = currentMessageTime;
            }

            // If chart is enabled, add another point to list
            if (enableChart) {
                let obj = {date: currentMessageTime, value: +event.data}
                dataArr.push(obj);
            }

            // If there is more than 1000 points, remove the earliest one
            if(dataArr.length > 1000) {
                dataArr.shift();
            } 
            
            if (enableChart) {
                // Ground case for rendering (first point only)
                if(dataArr.length == 1){
                    renderChart(dataArr)
                }
                // Render if nth message has been received and reset the counter
                if(reqCounter >= nthRequest){
                    reqCounter = 0;
                    appendPointToChart(dataArr);
                }
            }
        }})();

        
})
