var present = require('present');

var run = true;
var config = {
    allowedOrigins: ['http://localhost:1337', 'http://nenad-misic-bachelor.herokuapp.com', 'https://nenad-misic-bachelor.herokuapp.com'],
    frequency: 15000,
} 
var WebSocketServer = require('websocket').server;
var http = require('http');
 
var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Request-Method', '*');
	response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	response.setHeader('Access-Control-Allow-Headers', '*');
    switch (request.url) {
        case '/frequency':
            response.writeHead(200, {'Content-Type': 'application/json'});
            response.end(JSON.stringify({Value: config.frequency}));
            break;
        case '/slow':
            response.writeHead(200, {'Content-Type': 'application/json'});
            old = config.frequency;
            config.frequency *= 0.8;
            response.end(JSON.stringify({From: old, To:config.frequency}));
            break;
        case '/speed':
            response.writeHead(200, {'Content-Type': 'application/json'});
            old = config.frequency;
            config.frequency *= 1.2;
            response.end(JSON.stringify({From: old, To:config.frequency}));
            break;
        default:
            response.writeHead(404);
            response.end();
            break;
    }
});
server.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});
 
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
    return config.allowedOrigins.indexOf(origin)!=-1;
}
 
wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    
    var connection = request.accept();
    var timeBetweenTwoReq = 1000/config.frequency;
    var timeConnected = present()
    // var timeConnected = (new Date()).getTime();
    var lastTime = timeConnected;
    while (run) {
        let currentTime = present();
        if(currentTime >= lastTime + timeBetweenTwoReq){
            lastTime = currentTime;
            connection.sendUTF(Math.sin((currentTime - timeConnected)/1000))
        }
    }

    connection.on('close', function(reasonCode, description) {
        run = false;
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
