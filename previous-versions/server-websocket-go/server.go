package main

import (
	"flag"
	"log"
	"net/http"
    "time"
    "fmt"
    "math"
	"encoding/json"
	"github.com/gorilla/websocket"
)
var upgrader = websocket.Upgrader{} // use default options
var frequency = 10

type ChangedFrequency struct {
	From int
	To int
}


type CurrentFrequency struct {
	Value int
}

func getFrequency(w http.ResponseWriter, r *http.Request) {
	freq := CurrentFrequency{frequency}
	
	js, err := json.Marshal(freq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
  	w.Write(js)

	// w.Write([]byte(fmt.Sprintf("%f", frequency)))
}

func slowDown(w http.ResponseWriter, r *http.Request) {
	responseFreq := ChangedFrequency{frequency, frequency - int(math.Floor(float64(frequency)*0.2))}
	frequency = frequency - int(math.Floor(float64(frequency)*0.2))


	js, err := json.Marshal(responseFreq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
  	w.Write(js)

	// w.Write([]byte(fmt.Sprintf("%f", frequency)))
}

func speedUp(w http.ResponseWriter, r *http.Request) {
	responseFreq := ChangedFrequency{frequency, frequency + int(math.Floor(float64(frequency)*0.2))}
	frequency = frequency + int(math.Floor(float64(frequency)*0.2))

	js, err := json.Marshal(responseFreq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
  	w.Write(js)

	// w.Write([]byte(fmt.Sprintf("%f", frequency)))
}

func echo(w http.ResponseWriter, r *http.Request) {
	// begin_now := time.Now()
	// begin_nanos := begin_now.UnixNano()
	// begin_millis := begin_nanos/1000000

	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	cnt := 0
	ping := func() { 
		now := time.Now()
		nanos := now.UnixNano()
		millis := nanos/1000000
		c.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%f", math.Sin(float64(millis - begin_millis)/1000))))
		cnt += 1;
		if cnt % frequency == 0 {
			cnt = 0
			fmt.Println(millis)
		}
	}
	for {
		ping()
		select {
		case <- time.After(time.Duration(1000000000/frequency) * time.Nanosecond):
		}
	}


}


func schedule(what func(), delay time.Duration) chan bool {
    stop := make(chan bool)

    go func() {
        for {
            what()
            select {
            case <-time.After(delay):
            case <-stop:
                return
            }
        }
    }()

    return stop
}

func main() {
	var port = "8080"
	// var addr = flag.String("addr", port, "http service address")
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/", echo)
	http.HandleFunc("/frequency", getFrequency)
	http.HandleFunc("/slow", slowDown)
	http.HandleFunc("/speed", speedUp)
	log.Fatal(http.ListenAndServe(":"+port, nil))
	

    
}