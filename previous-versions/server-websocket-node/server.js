var config = {
    allowedOrigins: ['http://localhost:1337', 'http://nenad-misic-bachelor.herokuapp.com', 'https://nenad-misic-bachelor.herokuapp.com'],
    frequency: 60,
} 
var WebSocketServer = require('websocket').server;
var http = require('http');
 
var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});
 
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
    return config.allowedOrigins.indexOf(origin)!=-1;
}
 
wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    
    var connection = request.accept();
    var timeConnected = (new Date()).getTime();
    var lastTime = 0;
    var generator = setInterval(() => {
        let currentTime = (new Date()).getTime();
        console.log(lastTime - currentTime);
        lastTime = currentTime;
        connection.sendUTF(Math.sin((currentTime - timeConnected)/1000))
    }
    , 1000/config.frequency)

    connection.on('close', function(reasonCode, description) {
        clearInterval(generator);
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
